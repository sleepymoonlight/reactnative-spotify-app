import React from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, FlatList, StyleSheet} from 'react-native';
import ArtistItem from "../ArtistItem";

const style = StyleSheet.create({
    view: {
        flex: 1,
        width: '100%'
    },
    list: {
        paddingVertical: 8,
        flex: 2
    }
});

const ArtistsList = ({itemsList, onArtistClick}) => {
    return (
        <SafeAreaView style={style.view}>
            <FlatList
                data={itemsList}
                renderItem={({item}) => (
                    <ArtistItem
                        artist={item}
                        onClick={onArtistClick}
                    />
                )}
                keyExtractor={item => item.id}
                style={style.list}
                refreshing={true}
            />
        </SafeAreaView>
    )
};

ArtistsList.propTypes = {
    itemsList: PropTypes.array,
    onArtistClick: PropTypes.func
};

export default ArtistsList;