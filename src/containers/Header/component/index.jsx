import React, {useState} from 'react';
import {HEADERTYPES, ICONTYPE} from "../../../constants";
import PropTypes from 'prop-types';
import {View, StyleSheet, Text, TextInput, Keyboard} from 'react-native';
import HeaderButton from "../../../components/HeaderButton";

const style = StyleSheet.create({
    header: {
        zIndex: 10,
        backgroundColor: 'rgb(14,28,35)',
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        height: 56,
        paddingHorizontal: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        elevation: 6,
    },
    title: {
        color: '#ffffff',
        lineHeight: 23,
        fontSize: 20,
        fontWeight: "600"
    },
    searchButtons: {
        flexDirection: "row",
        alignItems: "center"
    },
    textInput: {
        color: "white",
        fontSize: 20,
        maxWidth: 200
    }
});

const HeaderComponent = ({header, canBackScreens, token, backScreen, fetchItemsList, clearItems}) => {
    const [searchInputOpened, setSearchInputOpened] = useState(false);
    const [currentHeader, setCurrentHeader] = useState(header);
    const [inputValue, setInputValue] = useState("");
    const showSearchButton = header.headerType === HEADERTYPES.SEARCH_HEADER && !searchInputOpened;
    const showBackButton = canBackScreens || searchInputOpened;

    if(currentHeader !== header){
        setSearchInputOpened(false);
        setCurrentHeader(header);
        setInputValue('');
    }

    const onBackButtonClick = () => {
        if(searchInputOpened){
            setSearchInputOpened(false);
            setInputValue('');
        } else {
            backScreen();
        }
    };

    const onSearchButtonClick = () => {
        setSearchInputOpened(true);
    };

    const onChangeInputText = (text) => {
        setInputValue(text);
    };

    const onClearButtonClick = () => {
        setInputValue('');
        clearItems();
    };

    const onApplyButtonClick = () => {
        if(!inputValue || inputValue.length <= 0){
            return;
        }

        Keyboard.dismiss();
        fetchItemsList(token, inputValue);
    };

    return (
        <View style={style.header}>

            <View style={style.searchButtons}>
                {showBackButton ?
                    <HeaderButton iconType={ICONTYPE.BACK} onPress={onBackButtonClick} paddingRight={true}/> :
                    null
                }

                {(searchInputOpened) ?
                    <TextInput
                        placeholder={header.searchPlaceHolder}
                        style={style.textInput}
                        underlineColorAndroid='transparent'
                        placeholderTextColor ="#a1a2a1"
                        onChangeText={onChangeInputText}
                        value={inputValue}
                        clearTextOnFocus={true}
                        onSubmitEditing={onApplyButtonClick}
                        selectTextOnFocus={true}
                    /> :
                    <Text style={style.title}>{header.title}</Text>
                }
            </View>

            {showSearchButton ?
                <View style={style.searchButtons}>
                    <HeaderButton iconType={ICONTYPE.SEARCH} onPress={onSearchButtonClick}/>
                </View> :
                null
            }

            {searchInputOpened ?
                <View style={style.searchButtons}>
                    <HeaderButton iconType={ICONTYPE.DONE} onPress={onApplyButtonClick}/>
                    <HeaderButton iconType={ICONTYPE.CLEAR} paddingLeft={true} onPress={onClearButtonClick}/>
                </View> :
                null
            }
        </View>
    )
};

HeaderComponent.propTypes = {
    header: PropTypes.object,
    canBackScreens: PropTypes.bool,
    backScreen: PropTypes.func,
    token: PropTypes.string,
    fetchItemsList: PropTypes.func,
    clearItems: PropTypes.func
};

export default HeaderComponent;