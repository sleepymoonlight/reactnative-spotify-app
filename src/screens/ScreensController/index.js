import React from 'react';
import {connect} from 'react-redux';
import {setScreen, getAccessToken} from '../../actions';
import PropTypes from 'prop-types';
import ScreensControllerComponent from './component';

const mapStateToProps = state => ({
  screen: state.screen,
});

const mapDispatchToProps = dispatch => ({
  setScreen(screen) {
    dispatch(setScreen(screen));
  },
  getAccessToken() {
    dispatch(getAccessToken(dispatch));
  },
});

const ScreensController = ({screen, setScreen, getAccessToken}) => (
  <ScreensControllerComponent
    screen={screen}
    setScreen={setScreen}
    getAccessToken={getAccessToken}
  />
);

ScreensController.propTypes = {
  screen: PropTypes.string,
  setScreen: PropTypes.func,
  getAccessToken: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScreensController);
