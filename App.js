import React from 'react';
import {Provider} from 'react-redux';
import store from './src/store';
import ScreensController from "./src/screens/ScreensController";

export default function App() {
  return (
      <Provider store={store}>
          <ScreensController />
      </Provider>
  );
}

