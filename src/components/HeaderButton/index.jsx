import React from 'react';
import {ICONTYPE} from "../../constants";
import PropTypes from 'prop-types';
import {View, TouchableOpacity, Image, StyleSheet} from 'react-native';

import clearIcon from './icons/clearIcon.png';
import doneIcon from './icons/doneIcon.png';
import searchIcon from './icons/searchIcon.png';
import backIcon from './icons/backIcon.png';

const HeaderButton = ({iconType = ICONTYPE.DONE, onPress = ()=> {}, paddingLeft = false, paddingRight = false}) => {
    let icon;
    switch (iconType) {
        case ICONTYPE.DONE:
            icon = doneIcon;
            break;
        case ICONTYPE.CLEAR:
            icon = clearIcon;
            break;
        case ICONTYPE.SEARCH:
            icon = searchIcon;
            break;
        case ICONTYPE.BACK:
            icon = backIcon;
            break;
        default:
            icon = doneIcon;
    }

    const style = StyleSheet.create({
        icon: {
            height: 24,
            width: 24,
        },
        touchable: {
            paddingLeft: paddingLeft ? 12 : 0,
            paddingRight: paddingRight ? 12 : 0,
        }
    });

    return (
        <TouchableOpacity onPress={onPress} style={style.touchable}>
            <View>
                <Image source={icon} style={style.icon}/>
            </View>
        </TouchableOpacity>
    )
};

HeaderButton.propTypes = {
    iconType: PropTypes.string,
    onPress: PropTypes.func,
    paddingLeft: PropTypes.bool,
    paddingRight: PropTypes.bool
};

export default HeaderButton;