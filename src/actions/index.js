import {
  SET_SCREEN,
  BACK_SCREEN,
  SET_HEADER,
  GET_ACCESS_TOKEN,
  FETCH_ITEMS_LIST,
  FETCH_ITEM,
  CLEAR_ITEMS,
} from './actionTypes';

import {APPCREDENTIALS} from '../constants';
import {encode as btoa} from 'base-64';
import formUrlencoded from '../helpers/formUrlencoded';
import {Alert} from 'react-native';

export const setScreen = (screen) => {
  return {
    type: SET_SCREEN,
    result: {
      screen: screen,
    },
  };
};

export const backScreen = () => {
  return {
    type: BACK_SCREEN,
  };
};

export const setHeader = (title, headerType, searchPlaceHolder) => {
  return {
    type: SET_HEADER,
    header: {
      title: title,
      headerType: headerType,
      searchPlaceHolder: searchPlaceHolder,
    },
  };
};

export const getAccessToken = (dispatch) => {
  fetchAccessToken(dispatch);

  return {
    type: GET_ACCESS_TOKEN,
    accessToken: '',
  };
};

const fetchAccessToken = (dispatch) => {
  const TOKEN = btoa(APPCREDENTIALS.CLIENTID + ':' + APPCREDENTIALS.SECRET);
  let details = {
    grant_type: 'client_credentials',
  };

  fetch('https://accounts.spotify.com/api/token', {
    method: 'POST',
    headers: {
      Authorization: 'Basic ' + TOKEN,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: formUrlencoded(details),
  })
    .then(result => result.json())
    .then(json => {
      if (!checkForError(json)) {
        dispatch({
          type: GET_ACCESS_TOKEN,
          accessToken: json.access_token,
        });
      }
    });
};

export const fetchItemsList = (dispatch, token, value) => {
  const query = new URLSearchParams({
    q: value,
    type: 'artist',
  }).toString();

  fetch('https://api.spotify.com/v1/search?' + query, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(result => result.json())
    .then(json => {
      if (!checkForError(json)) {
        dispatch({
          type: FETCH_ITEMS_LIST,
          isLoading: false,
          itemsList: json.artists.items,
        });
      }
    });

  return {
    type: FETCH_ITEMS_LIST,
    isLoading: true,
    itemsList: [],
  };
};

export const clearItems = () => {
  return {
    type: CLEAR_ITEMS,
    isLoading: false,
    itemsList: [],
  };
};

export const fetchItem = (dispatch, token, id, currentItem) => {
  fetch('https://api.spotify.com/v1/artists/' + id, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(result => result.json())
    .then(json => {
      if (!checkForError(json)) {
        dispatch({
          type: FETCH_ITEM,
          currentItem: Object.assign(currentItem, json),
        });
      }
    });

  return {
    type: FETCH_ITEM,
    currentItem: currentItem,
  };
};

const checkForError = json => {
  if (json.error) {
    Alert.alert(
      'Error!',
      json.error,
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );

    return true;
  }
  return false;
};
