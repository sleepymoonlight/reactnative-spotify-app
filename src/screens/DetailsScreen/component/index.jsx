import React from 'react';
import PropTypes from 'prop-types';
import {HEADERTYPES, SCREENS} from '../../../constants/index';
import {View, StyleSheet, Text, Image, ScrollView, TouchableOpacity, Linking } from 'react-native';
import unknownArtist from "../../../images/unknownArtist.gif";
import Stars from "../../../components/Stars";

const style = StyleSheet.create({
    screen: {
        backgroundColor: '#ffffff',
        flex: 1
    },
    cardImage: {
        width: '100%',
        height: 200,
        resizeMode: 'cover'
    },
    mainPart: {
        paddingHorizontal: 16,
        paddingVertical: 24
    },
    header: {
        fontSize: 24,
        fontWeight: '600',
        color: "#000000de"
    },
    oneLineStyle: {
        flexDirection: "row",
        paddingTop: 8,
        flexWrap: "wrap"
    },
    smallText: {
        fontSize: 12,
        color: "#0000008a"
    },
    secondary: {
        fontSize: 14,
        fontWeight: '600',
        color: "#000000de",
        textTransform: "capitalize",
        paddingTop: 14
    },
    description: {
        fontSize: 14,
        color: "#00000099",
        paddingTop: 8
    },
    subTitle: {
        paddingTop: 24,
        fontSize: 18,
        color: "#000000de"
    },
    chip: {
        fontSize: 14,
        color: "#000000de",
        backgroundColor: '#E1E2E1',
        marginRight: 8,
        paddingHorizontal: 16,
        paddingVertical: 8,
        borderRadius: 20,
        textTransform: "capitalize",
        marginBottom: 8
    },
    divider: {
        width: '100%',
        paddingTop: 16,
        borderBottomColor: "#E1E2E1",
        borderBottomWidth: 1
    },
    button: {
        color: "white",
        backgroundColor: '#0e1c23',
        fontSize: 14,
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 2,
        textTransform: "uppercase"
    },
    buttonContainer: {
        marginRight: 8
    }
});

const DetailsScreenComponent = ({setHeader, currentItem, setScreen}) => {
    setHeader(currentItem.name, HEADERTYPES.TITLE_HEADER);
    const popularity = Math.ceil(currentItem.popularity  / 20)  || 0;

    const genres = currentItem.genres.map((item, index) => {
       return <Text key={index} style={style.chip}>{item}</Text>
    });

    const onScroll = (e) => {
        console.log(e);
    };

    return (
        <ScrollView style={style.screen} onScroll={onScroll}>
            <Image source={!!currentItem.images[0] ? {uri: currentItem.images[0].url} : unknownArtist} style={style.cardImage}/>
            <View style={style.mainPart}>
                <Text style={style.header}>{currentItem.name}</Text>
                <View style={style.oneLineStyle}>
                    <Stars maxCount={5} count={popularity}/>
                    <Text style={style.smallText}>({currentItem.popularity})</Text>
                </View>
                <Text style={style.secondary}>{currentItem.type}</Text>
                <Text style={style.description}>Followers: {currentItem.followers.total}</Text>
                <View style={style.divider}/>
                <Text style={style.subTitle}>Genres</Text>
                <View style={style.oneLineStyle}>
                    {genres}
                </View>
                <View style={style.oneLineStyle}>
                    <TouchableOpacity
                        onPress={ ()=>{ Linking.openURL(currentItem.external_urls.spotify)}}
                        style={style.buttonContainer}
                    >
                        <Text style={style.button}>Visit artist</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={style.buttonContainer}
                        onPress={() => setScreen(SCREENS.MODAL_SCREEN)}
                    >
                        <Text style={style.button}>View QR</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    )
};

DetailsScreenComponent.propTypes = {
    setHeader: PropTypes.func,
    currentItem: PropTypes.object,
    setScreen: PropTypes.func
};

export default DetailsScreenComponent;
