import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import DetailsScreenComponent from './component';
import {setHeader, setScreen} from '../../actions';

const mapStateToProps = (state) => ({
  token: state.accessToken,
  currentItem: state.currentItem,
});

const mapDispatchToProps = (dispatch) => ({
  setHeader(title, headerType, searchPlaceHolder) {
    dispatch(setHeader(title, headerType, searchPlaceHolder));
  },
  setScreen(screen) {
    dispatch(setScreen(screen));
  },
});

const DetailsScreen = ({setHeader, currentItem, setScreen}) => (
  <DetailsScreenComponent
    setHeader={setHeader}
    currentItem={currentItem}
    setScreen={setScreen}
  />
);

DetailsScreen.propTypes = {
  setHeader: PropTypes.func,
  currentItem: PropTypes.object,
  setScreen: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DetailsScreen);
