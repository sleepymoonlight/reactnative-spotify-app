import React from 'react';
import PropTypes from 'prop-types';
import {View, Image, StyleSheet} from 'react-native';

import starBorder from '../../images/starBorder.png';
import starFilled from '../../images/starFilled.png';

const Stars = ({count, maxCount}) => {
    const style = StyleSheet.create({
        starsContainer: {
          flexDirection: 'row'
        },
        star: {
            width: 20,
            height: 20,
            tintColor: "#ffd500"
        }
    });

    let stars = [];
    for (let i = 0; i < maxCount; i++){
        stars.push(<Image source={i < count ? starFilled : starBorder} style={style.star} key={i}/>)
    }

    return (
        <View style={style.starsContainer}>
            {stars}
        </View>
    )
};

Stars.propTypes = {
    count: PropTypes.number,
    maxCount: PropTypes.number,
};

export default Stars;