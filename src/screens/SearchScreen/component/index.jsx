import React from 'react';
import PropTypes from 'prop-types';
import {HEADERTYPES, SCREENS} from '../../../constants/index';
import {View, Text} from 'react-native';
import ArtistsList from "../../../components/ArtistList";
import ScreenStyle from '../../ScreenStyle';

const SearchScreenComponent = ({setHeader, isLoading, itemsList, setScreen, fetchItem, token}) => {
    setHeader("Search song/artist", HEADERTYPES.SEARCH_HEADER, "Type name here...");
    const onArtistClick = (id) => {
        const item = itemsList.find(el => el.id === id);
        fetchItem(token, id, item);
        setScreen(SCREENS.DETAILS_SCREEN);
    };

    const showList = itemsList.length > 0;
    const itemToShow = showList ? <ArtistsList itemsList={itemsList} onArtistClick={onArtistClick}/> : <Text>No results found</Text>;

    return (
        <View style={ScreenStyle.screen}>
            {isLoading
                ? <Text>Loading...</Text>
                : itemToShow}
        </View>
    )
};

SearchScreenComponent.propTypes = {
    setHeader: PropTypes.func,
    isLoading: PropTypes.bool,
    itemsList: PropTypes.array,
    setScreen: PropTypes.func,
    fetchItem: PropTypes.func,
    token: PropTypes.string
};

export default SearchScreenComponent;
