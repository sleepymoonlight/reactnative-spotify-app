export const SET_SCREEN = 'SET_SCREEN';
export const BACK_SCREEN = 'BACK_SCREEN';
export const SET_HEADER = 'SET_HEADER';
export const GET_ACCESS_TOKEN = 'GET_ACCESS_TOKEN';
export const FETCH_ITEMS_LIST = 'FETCH_ITEMS_LIST';
export const FETCH_ITEM = 'FETCH_ITEM';
export const CLEAR_ITEMS = 'CLEAR_ITEMS';