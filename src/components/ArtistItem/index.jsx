import React from 'react';
import PropTypes from 'prop-types';
import {View, Image, TouchableOpacity, Text, StyleSheet} from 'react-native';
import Stars from "../Stars";
import unknownArtist from "../../images/unknownArtist.gif";

const style = StyleSheet.create({
    cardContainer: {
        marginHorizontal: 16,
        backgroundColor: '#ffffff',
        borderRadius: 4,
        marginBottom: 16,
        flex: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        elevation: 4,
    },
    topPart: {
        flexDirection: "row",
        flex: 1
    },
    cardImage: {
        width: 80,
        height: 80,
        borderTopLeftRadius: 4,
        resizeMode: 'cover'
    },
    mainTextPart: {
        paddingHorizontal: 16,
        justifyContent: 'center',
        borderBottomColor: "#E1E2E1",
        borderBottomWidth: 1,
        flex: 1,
        overflow: "hidden",
    },
    headerText: {
        color: "#000000de",
        fontSize: 18,
        fontWeight: '700',
    },
    categoryText: {
        color: "#00000099",
        fontSize: 16,
        fontWeight: '500'
    },
    bottomPart: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 16
    },
});

const ArtistItem = ({artist, onClick = () => {}}) => {
    const popularity = Math.ceil(artist.popularity  / 20)  || 0;

    const onArtistClick = () => {
        onClick(artist.id);
    };

    return (
        <TouchableOpacity style={style.cardContainer} onPress={onArtistClick}>
            <View>
                <View style={style.topPart}>
                    <Image source={!!artist.images[0] ? {uri: artist.images[0].url} : unknownArtist} style={style.cardImage}/>
                    <View style={style.mainTextPart}>
                        <Text style={style.headerText}>{artist.name}</Text>
                        <Text style={style.categoryText}>{artist.type}</Text>
                    </View>
                </View>
                <View style={style.bottomPart}>
                    <Text style={style.categoryText}>Popularity</Text>
                    <Stars count={popularity} maxCount={5}/>
                </View>
            </View>
        </TouchableOpacity>
    )
};

ArtistItem.propTypes = {
    artist: PropTypes.object,
    onClick: PropTypes.func
};

export default ArtistItem;