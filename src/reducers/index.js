import {
  SET_SCREEN,
  BACK_SCREEN,
  SET_HEADER,
  GET_ACCESS_TOKEN,
  FETCH_ITEMS_LIST,
  FETCH_ITEM,
  CLEAR_ITEMS,
} from '../actions/actionTypes';

export default (state = [], action) => {
  switch (action.type) {
    case SET_SCREEN: {
      state.screensStack.push(state.screen);
      state.canBackScreens = state.screensStack.length > 0;
      return Object.assign({}, state, {
        screen: action.result.screen,
      });
    }

    case BACK_SCREEN: {
      return Object.assign({}, state, {
        screen: state.screensStack.pop(),
        canBackScreens: state.screensStack.length > 0,
      });
    }

    case SET_HEADER: {
      return Object.assign({}, state, {
        header: action.header,
      });
    }

    case GET_ACCESS_TOKEN: {
      return Object.assign({}, state, {
        accessToken: action.accessToken,
      });
    }

    case FETCH_ITEMS_LIST: {
      return Object.assign({}, state, {
        itemsList: action.itemsList,
        isLoading: action.isLoading,
      });
    }

    case FETCH_ITEM: {
      return Object.assign({}, state, {
        currentItem: action.currentItem,
        isLoading: action.isLoading,
      });
    }

    case CLEAR_ITEMS: {
      return Object.assign({}, state, {
        itemsList: action.itemsList,
        isLoading: action.isLoading,
      });
    }

    default:
      return state;
  }
};
