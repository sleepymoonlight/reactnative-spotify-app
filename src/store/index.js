import {createStore} from 'redux';
import reducers from '../reducers';
import {SCREENS, HEADERTYPES} from '../constants';

const initialState = {
  screen: SCREENS.SEARCH_SCREEN,
  screensStack: [],
  canBackScreens: false,
  header: {
    headerType: HEADERTYPES.SEARCH_HEADER,
    title: 'Some title',
    searchPlaceHolder: '',
  },
  accessToken: '',
  isLoading: false,
  itemsList: [],
  currentItem: {},
};
const store = createStore(reducers, initialState);
export default store;
