import React from 'react';
import {connect} from 'react-redux';
import {backScreen, fetchItemsList, clearItems} from '../../actions';
import PropTypes from 'prop-types';
import HeaderComponent from "./component";

const mapStateToProps = (state) => ({
    canBackScreens: state.canBackScreens,
    header: state.header,
    token: state.accessToken
});

const mapDispatchToProps = (dispatch) => ({
    backScreen() {
        dispatch(backScreen());
    },
    fetchItemsList(token, value){
        dispatch(fetchItemsList(dispatch, token, value));
    },
    clearItems(){
        dispatch(clearItems());
    }
});

const Header = ({canBackScreens, header, token, backScreen, fetchItemsList, clearItems}) => (
    <HeaderComponent
        canBackScreens={canBackScreens}
        header={header}
        backScreen={backScreen}
        fetchItemsList={fetchItemsList}
        token={token}
        clearItems={clearItems}
    />
);

Header.propTypes = {
    backScreen: PropTypes.func,
    canBackScreens: PropTypes.bool,
    header: PropTypes.object,
    fetchItemsList: PropTypes.func,
    token: PropTypes.string,
    clearItems: PropTypes.func
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);