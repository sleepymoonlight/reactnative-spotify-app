import React from 'react';
import {SCREENS} from '../../../constants';
import PropTypes from 'prop-types';
import {View, StyleSheet} from 'react-native';
import Header from '../../../containers/Header';
import SearchScreen from '../../SearchScreen';
import DetailsScreen from '../../DetailsScreen';

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000',
  },
});

const ScreensControllerComponent = ({screen, getAccessToken}) => {
  getAccessToken();

  let screenToShow;
  if (screen === SCREENS.SEARCH_SCREEN) {
    screenToShow = <SearchScreen/>;
  } else if (screen === SCREENS.DETAILS_SCREEN) {
    screenToShow = <DetailsScreen/>;
  }

  return (
    <View style={style.container}>
      <Header/>
      {screenToShow}
    </View>
  );
};

ScreensControllerComponent.propTypes = {
  screen: PropTypes.string,
  setScreen: PropTypes.func,
  getAccessToken: PropTypes.func,
};

export default ScreensControllerComponent;
