import {StyleSheet} from "react-native";

const screenStyle = StyleSheet.create({
    screen: {
        backgroundColor: '#E1E2E1',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default screenStyle;