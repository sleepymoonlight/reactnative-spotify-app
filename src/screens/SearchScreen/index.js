import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import SearchScreenComponent from './component';
import {setHeader, setScreen, fetchItem} from '../../actions';

const mapStateToProps = (state) => ({
  token: state.accessToken,
  isLoading: state.isLoading,
  itemsList: state.itemsList,
});

const mapDispatchToProps = (dispatch) => ({
  setHeader(title, headerType, searchPlaceHolder) {
    dispatch(setHeader(title, headerType, searchPlaceHolder));
  },
  setScreen(screen) {
    dispatch(setScreen(screen));
  },
  fetchItem(token, id, artist) {
    dispatch(fetchItem(dispatch, token, id, artist));
  },
});

const SearchScreen = ({
  setHeader,
  isLoading,
  itemsList,
  setScreen,
  fetchItem,
  token,
}) => (
  <SearchScreenComponent
    setHeader={setHeader}
    isLoading={isLoading}
    itemsList={itemsList}
    setScreen={setScreen}
    fetchItem={fetchItem}
    token={token}
  />
);

SearchScreen.propTypes = {
  setHeader: PropTypes.func,
  isLoading: PropTypes.bool,
  itemsList: PropTypes.array,
  setScreen: PropTypes.func,
  token: PropTypes.string,
  fetchItem: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchScreen);
